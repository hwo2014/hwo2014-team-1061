using System;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

public class Bot {
    public static GameState gameState = null;

	public static void Main(string[] args) {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];
	    string track = "";
	    if (args.Count() > 4)
	    {
	        track = args[4];
	    }

	    Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

			new Bot(reader, writer, botName, botKey, track);
		}
	}

	private StreamWriter writer;

	List<CarPositions> carPositions = new List<CarPositions>();
    private double maxCentripetalAcceleration = 0.46;
    private string myCarColor = "red"; // default to red. Change if needed.
    List<double> velocities = new List<double>();
    List<double> accelerations = new List<double>();
    
    List<double> angles = new List<double>();
    List<double> angleVelocities = new List<double>();
    List<double> angleAccelerations = new List<double>();
    List<double> currentLapAngles = new List<double>();

    private double lastSentThrottle = 0.0;
    
    Dictionary<int, List<int>> laneSwitches = new Dictionary<int, List<int>>(); // lap && list of pieceIndexes (when the command was given)

    private double topSpeed = 10.0; // Top speed of the vehicle (can be altered by turbo)
    private double accelerationFactor = 0.2; // AccelerationFactor. Acceleration a = (throttle * 0.2) - (velocity / topSpeed) * 0,2; (can be altered by turbo)
    private double minMaxSpeed = double.MaxValue; // This is minimum speed that we never ever desire

    private bool turboAvailable = false;
    private int turboDuration;
    private bool turboOn = false;
    private double turboFactor = 1.0;
    private int turboOnUntil;

    private int crashCount = 0;
    private int latestGameTick = 0;

    private int latestVelocityFixedPiece = 0;
    private int latestVelocityFixedLane = 0;

    private bool gameInitialized = false;

    int longestConcurrentStraightPieces = 0;
    private List<Car> cars = new List<Car>();

    private Dictionary<int, double> MaxAngleForRadius = new Dictionary<int, double>();
    private Dictionary<int, Dictionary<int, double>> MaxVelocities = new Dictionary<int, Dictionary<int, double>>(); // pieceIndex => laneIndex => maxVelocity
    
    Bot(StreamReader reader, StreamWriter writer, string botName, string botKey, string track)
    {
	    this.writer = writer;
	    string line;

        if (track == "")
        {
            send(new Join(botName, botKey));
        }
        else
        {
            Log.Filename = string.Format("Log_{0}_{1}.txt", DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"), track);
            Log.WriteToFile(true);
            var create = new CreateRace(botName, botKey, track);
            send(create);
        }

	    while ((line = reader.ReadLine()) != null)
	    {
	        try
	        {
                var watch = new Stopwatch();
                watch.Start();
                MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
	            switch (msg.msgType)
	            {
	                case "yourCar":
	                    var yourCarMessage = JsonConvert.DeserializeObject<YourCarMessage>(line);
	                    myCarColor = yourCarMessage.data.color;
	                    Log.Write("My car color: " + myCarColor);
	                    break;
                    case "spawn":
                        var spawnMessage = JsonConvert.DeserializeObject<Spawn>(line);
	                    if (spawnMessage.gameTick == 0)
	                    {
                            Log.Write("Received a spawn message which does not have a gametick. Skipping.");
	                        break;
	                    }
	                    latestGameTick = spawnMessage.gameTick;
	                    if (spawnMessage.data.color == myCarColor)
	                    {
                            Log.Write("SPAWNED! Full speed ahead!");
	                        if (turboAvailable)
	                        {
                                UseTurbo();
	                        }
	                        else
	                        {
                                lastSentThrottle = 1.0;
                                send(new Throttle(1.0));
	                        }
	                    }
                        break;
                    case "carPositions":
	                    var carPositionsMessage = JsonConvert.DeserializeObject<CarPositions>(line);
	                    var myCar = carPositionsMessage.data.First(d => d.id.color == myCarColor);
                        carPositions.Add(carPositionsMessage);
	                    var velocity = CalculateVelocity();
	                    var currentRadius = GetCurrentPiece().RadiusForLane(myCar.piecePosition.lane.endLaneIndex, GetLanes());
	                    
	                    var currentAngle = CalculateAngle();
	                    if (!MaxAngleForRadius.ContainsKey(currentRadius))
	                    {
	                        MaxAngleForRadius[currentRadius] = Math.Abs(currentAngle);
	                    }
	                    else
	                    {
	                        if (Math.Abs(currentAngle) > MaxAngleForRadius[currentRadius])
	                        {
	                            MaxAngleForRadius[currentRadius] = Math.Abs(currentAngle);
	                        }
	                    }
	                    if (carPositionsMessage.gameTick == 0)
	                    {
	                        // We received a message without gameTick field. We should not reply to this
                            Log.Write("CarPositions message without gameTick received :(");
	                        break;
	                    }
	                    latestGameTick = carPositionsMessage.gameTick;
                        var currentPiece = gameState.data.race.track.pieces[myCar.piecePosition.pieceIndex];
                        // Increase max velocity if possible
	                    if (latestVelocityFixedPiece != myCar.piecePosition.pieceIndex &&
                            latestVelocityFixedLane != myCar.piecePosition.lane.endLaneIndex &&
                            velocity >= 0.98 * MaxVelocityForLane(myCar.piecePosition.pieceIndex, myCar.piecePosition.lane.endLaneIndex, GetLanes()) &&
	                        myCar.piecePosition.inPieceDistance > velocity * 3 &&
                            Math.Abs(currentAngle) < 59 &&
                            Math.Abs(angleVelocities.Last()) < 3 &&
                            lastSentThrottle > 0.1)
	                    {
                            double fixFactor = 1.0;
	                        var max = MaxVelocities[myCar.piecePosition.pieceIndex][myCar.piecePosition.lane.endLaneIndex];
	                        var angleVel = Math.Abs(angleVelocities.Last());
                            var angleAcc = angleAccelerations.Last();
	                        if (currentAngle < 0)
	                        {
	                            angleAcc = angleAcc * -1;
	                        }
	                        var tempAngle = Math.Abs(currentAngle);
                            if (tempAngle < 30)
                            {
                                fixFactor = 1.05;
                            }
                            else if (tempAngle < 40)
                            {
                                fixFactor = 1.03;
                            }
                            else if (tempAngle < 50 && velocity >= max * 0.985 && angleVel < 2)
                            {
                                fixFactor = 1.015;
                            }
                            else if (tempAngle < 55 && velocity >= max * 0.985 && angleVel < 1)
                            {
                                fixFactor = 1.006;
                            }
                            else if (tempAngle < 57 && angleVel < 0.6 && velocity >= max * 0.99 && angleAcc <= 0.2)
                            {
                                fixFactor = 1.0025;
                            }
                            else if (tempAngle < 58 && angleVel < 0.3 && velocity >= max * 0.99 && angleAcc <= 0.2)
                            {
                                fixFactor = 1.001;
                            }
                            else if (tempAngle < 59 && angleVel < 0.16 && velocity >= max * 0.99 && angleAcc <= 0.1)
                            {
                                fixFactor = 1.0005;
                            }

	                        if (Math.Abs(fixFactor - 1.0) > 0.000001)
	                        {
                                latestVelocityFixedPiece = myCar.piecePosition.pieceIndex;
                                latestVelocityFixedLane = myCar.piecePosition.lane.endLaneIndex;

                                MaxVelocities[myCar.piecePosition.pieceIndex][myCar.piecePosition.lane.endLaneIndex] = max * fixFactor;
                                Log.Write(string.Format("Adjusted max velocity for piece {2} and lane {3} to: {0}, max angle was: {1}",
                                    MaxVelocities[myCar.piecePosition.pieceIndex][myCar.piecePosition.lane.endLaneIndex], currentAngle,
                                    myCar.piecePosition.pieceIndex, myCar.piecePosition.lane.endLaneIndex));
                            }
                        }
	                    //Log.Write(string.Format("{0};{1};{2};{3}", GetCentripetalAcceleration(), myCar.angle, angleVelocities.LastOrDefault(), angleAccelerations.LastOrDefault()));

                        // 1. Calculate throttle (based on desired velocity on current piece and future pieces

                        var throttle = CalculateThrottle();
	                    Log.Write(string.Format("Estimated next velocity: {0} with throttle: {1}", CalculateNextVelocity(velocity, throttle), throttle));
                        //var throttle = CalculateThrottleForVelocity(2.0);

                        int currentEndLane = myCar.piecePosition.lane.endLaneIndex;
                        var maxVelocityForCurrentEndLane = MaxVelocityForLane(myCar.piecePosition.pieceIndex, currentEndLane, GetLanes());
	                    string maxVel = maxVelocityForCurrentEndLane > 1000000000 ? "inf" : maxVelocityForCurrentEndLane.ToString("0.00");
	                    //Log.Write(string.Format("{1};{0};{2};{3}", velocity, throttle, accelerations.LastOrDefault(), CalculateNextVelocity(velocity, throttle)));
                        Log.Write(string.Format("{5}: Rad: {3}, max vel: {4}, Vel: {0}, thr: {1:0.00}, ang: {2:0.00}", velocity, throttle, currentAngle, currentRadius, maxVel, myCar.piecePosition.pieceIndex));

                        // 2. Decide wether or not we want to switch now
	                    string direction;
	                    if (ShouldISwitchLane(out direction))
	                    {
	                        var lap = myCar.piecePosition.lap;
	                        if (!laneSwitches.Any() ||
                                (!laneSwitches.ContainsKey(lap) ||
                                !laneSwitches[lap].Contains(myCar.piecePosition.pieceIndex)))
	                        {
	                            if (!laneSwitches.ContainsKey(lap))
	                            {
                                    laneSwitches[lap] = new List<int>();
	                            }
	                            laneSwitches[lap].Add(myCar.piecePosition.pieceIndex);
                                Log.Write(string.Format("SWITCH TO: {0}", direction));
                                send(new Switch(direction));
	                            break;
	                        }
	                    }

                        // 3. Decide wether or not we want to TURBO BOOST now
                        if (turboAvailable)
                        {
                            if (ShouldIUseTurbo())
                            {
                                UseTurbo();
                                break;
                            }
                        }

	                    lastSentThrottle = throttle;
                        send(new Throttle(throttle));
	                    break;
	                case "join":
	                    Log.Write("Joined");
	                    break;
	                case "gameInit":
	                    gameState = JsonConvert.DeserializeObject<GameState>(line);
	                    if (gameState.data.race.raceSession.durationMs != 0)
	                    {
                            // Qualification round. Now is a good time to try different things (but keep in mind that we still need as fast lap as possible)
                            Log.Write("Qualification round!");
	                    }
	                    else
	                    {
	                        Log.Write("The race is on!");
                            // If we already had qualifications round, then there is no need to calculate the same stuff again
	                        if (gameInitialized)
	                        {
                                break;
                            }
	                    }
	                    Log.Write(string.Format("Race init: {0}, Number of pieces: {1}", gameState.data.race.track.id, gameState.data.race.track.pieces.Count));
                        Log.Write(string.Format("Lanes:{1}{0}", JsonConvert.SerializeObject(GetLanes()), Environment.NewLine));
	                    CalculateMaxVelocities();
	                    int currentConcurrentStraightPieces = 0;
	                    var pieceIndex = 0;
	                    foreach (var piece in gameState.data.race.track.pieces)
	                    {
                            Log.Write(string.Format("Piece {0}: Rad: {2}, ang: {3}, switch: {1}, len: {4}", pieceIndex++, piece.@switch, piece.radius, piece.angle, piece.length));
	                        if (!piece.IsStraight())
	                        {
                                // End of an era
	                            if (currentConcurrentStraightPieces > longestConcurrentStraightPieces)
	                            {
	                                longestConcurrentStraightPieces = currentConcurrentStraightPieces;
	                            }
	                            currentConcurrentStraightPieces = 0;
                            }
	                        else
	                        {
	                            currentConcurrentStraightPieces++;
	                        }
	                    }
	                    if (currentConcurrentStraightPieces > 0)
	                    {
                            // Check this straight line to the end
	                        int index = 0;
	                        while (!gameState.data.race.track.pieces[index].IsStraight())
	                        {
	                            currentConcurrentStraightPieces++;
	                            index++;
	                        }
	                        if (currentConcurrentStraightPieces > longestConcurrentStraightPieces)
	                        {
	                            longestConcurrentStraightPieces = currentConcurrentStraightPieces;
	                        }
	                    }
	                    cars = gameState.data.race.cars;
	                    gameInitialized = true;
                        Log.Write(string.Format("Longest concurrent straight pieces: {0}", longestConcurrentStraightPieces));
	                    break;
	                case "gameEnd":
	                    Log.Write("Race ended");
                        Log.Write("Max angles for radiuses:");
	                    foreach (KeyValuePair<int, double> maxAngleForRadius in MaxAngleForRadius)
	                    {
	                        Log.Write(string.Format("Rad: {0}: {1}", maxAngleForRadius.Key, maxAngleForRadius.Value));
	                    }
                        Log.Write(string.Format("Crash count: {0}", crashCount));
	                    break;
	                case "gameStart":
	                    Log.Write("Race starts");
                        lastSentThrottle = 1.0;
	                    send(new Throttle(1.0));
	                    break;
	                case "lapFinished":
	                    var lapFinished = JsonConvert.DeserializeObject<LapFinished>(line);
	                    if (lapFinished.data.car.color == myCarColor)
	                    {
	                        Log.Write("Lap finished. Lap time: " + lapFinished.data.lapTime.millis + "ms");
	                        // Check if we need to adjust curve velocities
                            //var maxAngle = currentLapAngles.Max();
                            //var minAngle = currentLapAngles.Min();
                            //if (Math.Abs(minAngle) > maxAngle)
                            //{
                            //    maxAngle = Math.Abs(minAngle);
                            //}
                            //double fixFactor = 1.0;
                            //if (maxAngle < 20)
                            //{
                            //    fixFactor = 1.25;
                            //}
                            //else if (maxAngle < 30)
                            //{
                            //    fixFactor = 1.17;
                            //}
                            //else if (maxAngle < 40)
                            //{
                            //    fixFactor = 1.08;
                            //}
                            //else if (maxAngle < 50)
                            //{
                            //    fixFactor = 1.04;
                            //}
                            //else if (maxAngle < 55)
                            //{
                            //    fixFactor = 1.015;
                            //}
                            //else if (maxAngle < 57)
                            //{
                            //    fixFactor = 1.007;
                            //}
                            //else if (maxAngle < 59)
                            //{
                            //    fixFactor = 1.005;
                            //}
                            ////else if (maxAngle > 59)
                            ////{
                            ////    fixFactor = 0.998;
                            ////}
                            //maxCentripetalAcceleration = maxCentripetalAcceleration * fixFactor;
                            //Log.Write(string.Format("Adjusted maxCentripetalAcceleration to: {0}, max angle was: {1}", maxCentripetalAcceleration, maxAngle));
                            //currentLapAngles.Clear();
                            //if (fixFactor - 1.0 > 0.0001)
                            //{
                            //    // TODO: Fix MinMaxSpeed if needed
                            //    CalculateMaxVelocities();
                            //}
	                    }
	                    if (lapFinished.gameTick != 0)
	                    {
	                        latestGameTick = lapFinished.gameTick;
                            send(new Ping());
	                    }
	                    break;
                    case "crash":
	                    var crashMessage = JsonConvert.DeserializeObject<CrashMessage>(line);
	                    if (crashMessage.data.color == myCarColor)
	                    {
                            // Fuuuuu!! We crashed :( Write some log data
	                        crashCount++;
	                        var pos = GetMyLastPosition();
                            Log.Write(string.Format("CRASH BOOM BANG! Velocity: {0}, angle: {1}, piece: {2}, piecePos: {3}, lane: {4}", velocities.Last(), pos.angle, pos.piecePosition.pieceIndex, pos.piecePosition.inPieceDistance, pos.piecePosition.lane.endLaneIndex));
	                        double v2 = Math.Pow(velocities.Last(), 2.0);
	                        int r = gameState.data.race.track.pieces[pos.piecePosition.pieceIndex].RadiusForLane(pos.piecePosition.lane.endLaneIndex, gameState.data.race.track.lanes);
                            Log.Write(string.Format("Centripetal acceleration: a = v^2 / r => {0} = {1}^2 / {2}", v2 / r, velocities.Last(), r));
	                        if (velocities.Last() < minMaxSpeed && crashCount < 3 || crashCount < 3 && pos.piecePosition.lap == 1)
	                        {
                                // We probably just started on a slippery road. Try to fix maxCentripetalAcceleration
	                            maxCentripetalAcceleration = maxCentripetalAcceleration * 0.95;
                                Log.Write(string.Format("Fixed centripetal acceleration to be {0}", maxCentripetalAcceleration));
                                MaxVelocities.Clear();
                                CalculateMaxVelocities();
                            }
	                        else
	                        {
                                //var angleVelocity = pos.angle - GetMySecondLastPosition().angle;
                                //var fixFactor = 1 - (angleVelocity / 10.0);
	                            MaxVelocities[pos.piecePosition.pieceIndex][pos.piecePosition.lane.endLaneIndex] =
	                                MaxVelocities[pos.piecePosition.pieceIndex][pos.piecePosition.lane.endLaneIndex]*0.96;

	                            var tempMinMax = Double.MaxValue;
	                            foreach (KeyValuePair<int, Dictionary<int, double>> keyValuePair in MaxVelocities)
	                            {
	                                foreach (KeyValuePair<int, double> valuePair in keyValuePair.Value)
	                                {
	                                    if (valuePair.Value < tempMinMax)
	                                    {
	                                        tempMinMax = valuePair.Value;
	                                    }
	                                }
	                            }
	                            minMaxSpeed = tempMinMax;
	                        }
	                    }
	                    if (crashMessage.gameTick != 0)
	                    {
                            latestGameTick = crashMessage.gameTick;
                            send(new Ping());
                        }
	                    break;
                    case "turboAvailable":
                        var turboMessage = JsonConvert.DeserializeObject<TurboAvailable>(line);
	                    turboFactor = turboMessage.data.turboFactor;
	                    turboDuration = turboMessage.data.turboDurationTicks;
                        Log.Write(string.Format("TURBO! Let's boost up! Duration: {0}, Factor: {1}", turboMessage.data.turboDurationTicks, turboMessage.data.turboFactor));
	                    turboAvailable = true;
	                    break;
                    case "turboStart":
                        var turboStartMessage = JsonConvert.DeserializeObject<TurboStart>(line);
                        if (turboStartMessage.data.color == myCarColor)
	                    {
                            Log.Write("Turbo ON!");
                            turboOn = true;
                            turboOnUntil = latestGameTick + turboDuration;
                        }
                        if (turboStartMessage.gameTick != 0)
	                    {
                            send(new Ping());
	                    }
	                    break;
                    case "turboEnd":
                        var turboEndMessage = JsonConvert.DeserializeObject<TurboEnd>(line);
                        if (turboEndMessage.data.color == myCarColor)
	                    {
                            Log.Write("Turbo OFF!");
                            turboOn = false;
                        }
                        if (turboEndMessage.gameTick != 0)
	                    {
                            send(new Ping());
	                    }
                        break;
                    default:
	                    Log.Write(JsonConvert.SerializeObject(msg));
	                    break;
	            }
                Log.Write(string.Format("Execution time: {0}ms. Gametick: {1}", watch.ElapsedMilliseconds, latestGameTick));
                Log.Flush();
	        }
	        catch (Exception e)
	        {
	            Log.Write(e.Message);
	            Log.Write(e.StackTrace);
                Log.Flush();
	        }
	    }
	}

    private double CalculateAngle()
    {
        if (!carPositions.Any())
        {
            return 0.0;
        }
        var myCar = carPositions.Last().data.First(d => d.id.color == myCarColor);

        if (angles.Any())
        {
            var velocity = myCar.angle - angles.Last();
            if (angleVelocities.Any())
            {
                angleAccelerations.Add(velocity - angleVelocities.Last());
            }
            angleVelocities.Add(velocity);
        }
        angles.Add(myCar.angle);
        currentLapAngles.Add(myCar.angle);

        return myCar.angle;
    }

    private void CalculateMaxVelocities()
    {
        minMaxSpeed = double.MaxValue;
        for (int pieceIndex = 0; pieceIndex < gameState.data.race.track.pieces.Count; pieceIndex++)
        {
            if (!MaxVelocities.ContainsKey(pieceIndex))
            {
                MaxVelocities[pieceIndex] = new Dictionary<int, double>();
            }
            for (int laneIndex = 0; laneIndex < gameState.data.race.track.lanes.Count; laneIndex++)
            {
                var trackLane = gameState.data.race.track.lanes[laneIndex];
                var maxVelocity = MaxVelocityForLane(pieceIndex, trackLane.index, gameState.data.race.track.lanes);
                if (maxVelocity < minMaxSpeed)
                {
                    minMaxSpeed = maxVelocity;
                }
                MaxVelocities[pieceIndex][laneIndex] = maxVelocity;
            }
        }
        Log.Write(string.Format("MinMaxSpeed set to: {0}", minMaxSpeed));
    }

    private CarPosData GetMyLastPosition()
    {
        return carPositions.Last().data.First(c => c.id.color == myCarColor);
    }

    private CarPosData GetMySecondLastPosition()
    {
        return carPositions[carPositions.Count - 2].data.First(c => c.id.color == myCarColor);
    }

    private bool ShouldIUseTurbo()
    {
        var pos = GetMyLastPosition();
        // If next piece starts a longest straight line in the track AND we are no more than two ticks away from it, boost up!
        if (pos.piecePosition.inPieceDistance + (velocities.Last() * 2.0) > CurrentLaneLength())
        {
            int nextPieceIndex = pos.piecePosition.pieceIndex;
            Piece piece;
            if (IsLastLap() && pos.piecePosition.pieceIndex > 0)
            {
                int startTurboAtIndex = pos.piecePosition.pieceIndex; // Default to current piece. Change ideal turbo place if needed.
                // Find longest straight route to use turbo at
                int tempStartTurboAtIndex = pos.piecePosition.pieceIndex;
                int longestStraightRoute = 0;
                int currentStraightRoute = 0;
                piece = GetCurrentPiece();
                while (nextPieceIndex != 0)
                {
                    if (piece.IsStraight())
                    {
                        currentStraightRoute++;
                        if (currentStraightRoute > longestStraightRoute)
                        {
                            longestStraightRoute = currentStraightRoute;
                            startTurboAtIndex = tempStartTurboAtIndex;
                        }
                    }
                    else
                    {
                        currentStraightRoute = 0;
                        tempStartTurboAtIndex = nextPieceIndex + 1; // Maybe next piece is straight again
                    }
                    piece = gameState.data.race.track.GetNextPiece(nextPieceIndex, out nextPieceIndex);
                }
                if (startTurboAtIndex == pos.piecePosition.pieceIndex)
                {
                    return true;
                }
            }
            else
            {
                piece = gameState.data.race.track.GetNextPiece(pos.piecePosition.pieceIndex, out nextPieceIndex);
                if (piece.IsStraight())
                {
                    bool longestStarting = true;
                    for (int straightCount = 1;
                        straightCount < longestConcurrentStraightPieces;
                        straightCount++)
                    {
                        if (
                            !gameState.data.race.track.GetNextPiece(nextPieceIndex, out nextPieceIndex)
                                .IsStraight())
                        {
                            longestStarting = false;
                        }
                    }
                    if (longestStarting)
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private Piece GetCurrentPiece()
    {
        return gameState.data.race.track.pieces[GetMyLastPosition().piecePosition.pieceIndex];
    }

    private void UseTurbo()
    {
        Log.Write("TURBO BOOST!!");
        send(new Turbo("TURBO BOOST"));
        turboAvailable = false;
    }

    private double CurrentLaneLength()
    {
        var pos = GetMyLastPosition();
        var piece = GetCurrentPiece();
        var lanes = GetLanes();

        return piece.LengthForLane(pos.piecePosition.lane.startLaneIndex, pos.piecePosition.lane.endLaneIndex, lanes);
    }

    private List<TrackLane> GetLanes()
    {
        return gameState.data.race.track.lanes;
    }

    private bool ShouldISwitchLane(out string direction)
    {
        var pos = GetMyLastPosition();
        var currentPieceIndex = pos.piecePosition.pieceIndex;
        int nextPieceIndex;
        var nextPiece = gameState.data.race.track.GetNextPiece(currentPieceIndex, out nextPieceIndex);
        if (nextPiece.HasSwitch())
        {
            double lengthForCurrent = Double.MaxValue;
            double lengthForRight = Double.MaxValue;
            double lengthForLeft = Double.MaxValue;

            // Calculate current lane route length;
            Piece tempPiece;
            int tempPieceIndex = nextPieceIndex;
            var laneIndex = pos.piecePosition.lane.endLaneIndex;
            var totalLength = nextPiece.LengthForLane(laneIndex, GetLanes()); // Full length of switch piece when we are staying on this lane
            while (!(tempPiece = gameState.data.race.track.GetNextPiece(tempPieceIndex, out tempPieceIndex)).HasSwitch())
            {
                // TODO make calculations two switches away
                totalLength += tempPiece.LengthForLane(laneIndex, GetLanes());
            }
            totalLength += (tempPiece.LengthForLane(laneIndex, GetLanes()) / 2);
            lengthForCurrent = totalLength;

            // check for both right and left switches
            if (GetLanes().Count > pos.piecePosition.lane.endLaneIndex + 1)
            {
                // We can switch to the right
                tempPieceIndex = nextPieceIndex;
                var newLaneIndex = laneIndex + 1;
                totalLength = nextPiece.LengthForLane(laneIndex, newLaneIndex, GetLanes()); // Let's count the length of the piece when we are switching to the right
                while (!(tempPiece = gameState.data.race.track.GetNextPiece(tempPieceIndex, out tempPieceIndex)).HasSwitch())
                {
                    totalLength += tempPiece.LengthForLane(newLaneIndex, GetLanes());
                }
                totalLength += (tempPiece.LengthForLane(newLaneIndex, GetLanes()) / 2);
                lengthForRight = totalLength;
            }
            if (pos.piecePosition.lane.endLaneIndex > 0)
            {
                // We can switch to the left
                tempPieceIndex = nextPieceIndex;
                var newLaneIndex = laneIndex - 1;
                totalLength = nextPiece.LengthForLane(laneIndex, newLaneIndex, GetLanes()); // Let's count the length of the piece when we are switching to the left
                while (!(tempPiece = gameState.data.race.track.GetNextPiece(tempPieceIndex, out tempPieceIndex)).HasSwitch())
                {
                    totalLength += tempPiece.LengthForLane(newLaneIndex, GetLanes());
                }
                totalLength += (tempPiece.LengthForLane(newLaneIndex, GetLanes()) / 2);
                lengthForLeft = totalLength;
            }
            if (lengthForLeft < lengthForCurrent || lengthForRight < lengthForCurrent)
            {
                // We should totally switch lanes
                direction = lengthForLeft < lengthForRight ? "Left" : "Right";
                return true;
            }
        }
        direction = "";
        return false;
    }

    private double CalculateThrottle()
    {
        var pos = GetMyLastPosition();

        if (velocities.Count < 5 && accelerations.Count < 5)
        {
            // Full throttle
            return 1.0;
        }
        var lastVelocity = velocities.Last();

        var lastAngle = pos.angle;
        int currentStartLane = pos.piecePosition.lane.startLaneIndex;
        int currentEndLane = pos.piecePosition.lane.endLaneIndex;
        var maxVelocityForCurrentEndLane = MaxVelocityForLane(pos.piecePosition.pieceIndex, currentEndLane, GetLanes());

        // Check the positions of other cars (maybe we should give them a friendly bumb :)
        var enemy = GetNearestEnemyAhead();
        if (enemy != null)
        {
            var distanceToEnemy = DistanceBetweenPositions(pos, enemy);
            var enemyVelocity = 0.0;
            if (carPositions.Count > 1)
            {
                var secondLastEnemyPos = carPositions[carPositions.Count - 2].data.First(c => c.id.color == enemy.id.color);
                enemyVelocity = DistanceBetweenPositions(secondLastEnemyPos.piecePosition, enemy.piecePosition);
            }
            if (
                distanceToEnemy < lastVelocity && // We are less than one tick away
                lastAngle < 40 && // We are not out of control
                maxVelocityForCurrentEndLane > CalculateNextVelocity(lastVelocity, 1.0)) // And we don't want to get out of control
            {
                Log.Write(string.Format("CHAAAARGE!! Car color '{0}' is straight ahead! Dist: {1}, vel: {2}", enemy.id.color, distanceToEnemy, enemyVelocity));
                return 1.0;
            }
            Log.Write(string.Format("Enemy ahead. Car color '{0}'. Dist: {1}, vel: {2}", enemy.id.color, distanceToEnemy, enemyVelocity));
        }

        enemy = GetNearestEnemyBehind();
        if (enemy != null)
        {
            var enemyVelocity = 0.0;
            if (carPositions.Count > 1)
            {
                var secondLastEnemyPos = carPositions[carPositions.Count - 2].data.First(c => c.id.color == enemy.id.color);
                enemyVelocity = DistanceBetweenPositions(secondLastEnemyPos.piecePosition, enemy.piecePosition);
            }
            var distanceToEnemy = DistanceBetweenPositions(pos, enemy);
            Log.Write(string.Format("Enemy after me. Car color '{0}'. Dist: {1}, vel: {2}", enemy.id.color, distanceToEnemy, enemyVelocity));
        }

        var secondLastAngle = GetMySecondLastPosition().angle;
        var angleVelocity = lastAngle - secondLastAngle;
        if (Math.Abs(lastAngle) > 25 && Math.Abs(angleVelocity) > 5)
        {
            return 0.0;
        }
        if (Math.Abs(lastAngle) > 30 && Math.Abs(angleVelocity) > 4.5)
        {
            return 0.0;
        }
        if (Math.Abs(lastAngle) > 35 && Math.Abs(angleVelocity) > 3.5)
        {
            return 0.0;
        }
        if (Math.Abs(lastAngle) > 40 && Math.Abs(angleVelocity) > 3)
        {
            return 0.0;
        }
        if (Math.Abs(lastAngle) > 45 && Math.Abs(angleVelocity) > 2.5)
        {
            return 0.0;
        }
        if (Math.Abs(lastAngle) > 50 && Math.Abs(angleVelocity) > 2)
        {
            return 0.0;
        }
        if (Math.Abs(lastAngle) > 52 && Math.Abs(angleVelocity) > 1.6)
        {
            return 0.0;
        }
        if (Math.Abs(lastAngle) > 54 && Math.Abs(angleVelocity) > 1.4)
        {
            return 0.0;
        }
        if (Math.Abs(lastAngle) > 55 && Math.Abs(angleVelocity) > 1.2)
        {
            return 0.0;
        }
        if (Math.Abs(lastAngle) > 56 && Math.Abs(angleVelocity) > 1)
        {
            return 0.0;
        }
        if (Math.Abs(lastAngle) > 57 && Math.Abs(angleVelocity) > 0.6)
        {
            return 0.0;
        }
        if (Math.Abs(lastAngle) > 58 && Math.Abs(angleVelocity) > 0.4)
        {
            return 0.0;
        }
        if (Math.Abs(lastAngle) > 59 && Math.Abs(angleVelocity) > 0.2)
        {
            return 0.0;
        }
        if (Math.Abs(lastAngle) > 59.5 && Math.Abs(angleVelocity) > 0.0)
        {
            return 0.0;
        }

        if (CalculateNextVelocity(lastVelocity, 1.0) <= minMaxSpeed)
        {
            return 1.0;
        }

        var desiredSpeed = CalculateNextVelocity(lastVelocity, 1.0);
        var maxVelocityForCurrentStartLane = MaxVelocityForLane(pos.piecePosition.pieceIndex, currentStartLane, GetLanes());
        var maxVelocityForCurrentLane = maxVelocityForCurrentStartLane > maxVelocityForCurrentEndLane
            ? maxVelocityForCurrentEndLane
            : maxVelocityForCurrentStartLane;
        if (maxVelocityForCurrentLane >= desiredSpeed)
        {
            // Calculate that if we accelerate now, will we be able to deccelerate before future curves
            if (CanWeUseThrottleSafely(1.0))
            {
                return 1.0;
            }
        }

        var throttleForVelocity = CalculateThrottleForVelocity(maxVelocityForCurrentLane);
        Log.Write(string.Format("Calculating throttle for velocity {0}", maxVelocityForCurrentLane));
        if (CanWeUseThrottleSafely(throttleForVelocity))
        {
            return throttleForVelocity;
        }

        // Should we deccelerate or keep the same pace?
        if (lastVelocity > minMaxSpeed)
        {
            return 0.0;
        }

        return minMaxSpeed / topSpeed;
    }

    private CarPosData GetNearestEnemyBehind()
    {
        var pos = GetMyLastPosition();
        var enemyCars = GetEnemyCarsOnSameLane(); // Positions of the cars on same lane as me
        if (!enemyCars.Any())
        {
            return null;
        }

        var nearest = enemyCars.First();
        var nearestDistance = DistanceBetweenPositions(nearest, pos);
        foreach (CarPosData carPosData in enemyCars)
        {
            var distance = DistanceBetweenPositions(carPosData, pos);
            if (distance < nearestDistance)
            {
                nearestDistance = distance;
                nearest = carPosData;
            }
        }
        return nearest;
    }

    private CarPosData GetNearestEnemyAhead()
    {
        var pos = GetMyLastPosition();
        var enemyCars = GetEnemyCarsOnSameLane(); // Positions of the cars on same lane as me
        if (!enemyCars.Any())
        {
            return null;
        }

        var nearest = enemyCars.First();
        var nearestDistance = DistanceBetweenPositions(pos, nearest);
        foreach (CarPosData carPosData in enemyCars)
        {
            var distance = DistanceBetweenPositions(pos, carPosData);
            if (distance < nearestDistance)
            {
                nearestDistance = distance;
                nearest = carPosData;
            }
        }
        return nearest;
    }

    private double DistanceBetweenPositions(CarPosData pos1, CarPosData pos2)
    {
        double distance = DistanceBetweenPositions(pos1.piecePosition, pos2.piecePosition);

        // Remember to take car length into account
        Car inPos1 = cars.First(c => c.id.color == pos1.id.color);
        distance -= inPos1.dimensions.guideFlagPosition;

        Car inPos2 = cars.First(c => c.id.color == pos2.id.color);
        distance -= (inPos2.dimensions.length - inPos2.dimensions.guideFlagPosition);

        return distance;
    }

    private double DistanceBetweenPositions(PiecePosition pos1, PiecePosition pos2)
    {
        double distance = 0.0;
        if (pos1.pieceIndex == pos2.pieceIndex && pos2.inPieceDistance > pos1.inPieceDistance)
        {
            distance = pos2.inPieceDistance - pos1.inPieceDistance;
        }
        else
        {
            var currentPiece = GetPiece(pos1.pieceIndex);
            distance = currentPiece.LengthForLane(pos1.lane.endLaneIndex, GetLanes()) - pos1.inPieceDistance;
            var nextIndex = gameState.data.race.track.GetNextPieceIndex(pos1.pieceIndex);
            while (nextIndex != pos2.pieceIndex)
            {
                distance += GetPiece(nextIndex).LengthForLane(pos1.lane.endLaneIndex, GetLanes());
                nextIndex = gameState.data.race.track.GetNextPieceIndex(nextIndex);
            }
            // Now we are on the same piece, so just add up pos2 inPieceDistance
            distance += pos2.inPieceDistance;
        }

        return distance;
    }

    private List<CarPosData> GetEnemyCarsOnSameLane()
    {
        var myPos = GetMyLastPosition();
        return carPositions.Last().data.Where(p => p.piecePosition.lane.endLaneIndex == myPos.piecePosition.lane.endLaneIndex && p.id.color != myCarColor).ToList();
    }

    // a = (throttle * accFactor) - (velocity / topSpeed) * accFactor
    // => throttle * accFactor = a + (velocity / topSpeed) * accFactor
    // => throttle = a / accFactor + velocity / topSpeed
    private double CalculateThrottleForVelocity(double desiredVelocity)
    {
        var lastVelocity = velocities.Last();
        double currentAccelerationFactor = accelerationFactor;
        double currentTopSpeed = topSpeed;

        if (turboOn)
        {
            currentAccelerationFactor = currentAccelerationFactor * turboFactor;
            currentTopSpeed = currentTopSpeed * turboFactor;
        }

        double desiredAcceleration = desiredVelocity - lastVelocity;
        var throttle = desiredAcceleration / currentAccelerationFactor + lastVelocity / currentTopSpeed;
        if (throttle < 0.0)
        {
            return 0.0;
        }
        if (throttle > 1.0)
        {
            return 1.0;
        }
        return throttle;
    }

    private bool IsLastLap()
    {
        return gameState.data.race.raceSession.laps == GetMyLastPosition().piecePosition.lap + 1;
    }

    private bool CanWeUseThrottleSafely(double throttle)
    {
        var lastVelocity = velocities.Last();
        var pos = GetMyLastPosition();
        Piece currentPiece = gameState.data.race.track.pieces[pos.piecePosition.pieceIndex];
        int currentEndLane = pos.piecePosition.lane.endLaneIndex;
        int currentPieceIndex = pos.piecePosition.pieceIndex;
        double currentInPieceDistance = pos.piecePosition.inPieceDistance;
        int ticksInTheFuture = 1;
        double nextVelocity = CalculateNextVelocity(lastVelocity, throttle, ticksInTheFuture++);

        while (nextVelocity > minMaxSpeed)
        {
            var pieceLength = currentPiece.LengthForLane(currentEndLane, GetLanes());
            if (currentInPieceDistance + nextVelocity > pieceLength)
            {
                // Piece changes
                currentPiece = gameState.data.race.track.GetNextPiece(currentPieceIndex, out currentPieceIndex);
                if (IsLastLap() && currentPieceIndex == 0)
                {
                    // This is the last lap and we can see the finish line! Go for it!
                    return true;
                }
                currentInPieceDistance = (currentInPieceDistance + nextVelocity) - pieceLength;
                if (nextVelocity > MaxVelocityForLane(currentPieceIndex, currentEndLane, GetLanes()))
                {
                    // Fuuu! We can not!
                    return false;
                }
            }
            else
            {
                currentInPieceDistance = currentInPieceDistance + nextVelocity;
            }
            // After first acceleration, we would of course deccelerate until safe speed because we want to drive safe :)
            nextVelocity = CalculateNextVelocity(nextVelocity, 0.0, ticksInTheFuture++);
        }
        return true;
    }

    // a = v^2 / r => a*r = v^2 => v = sqrt(a*r)
    public double MaxVelocityForLane(int pieceIndex, int laneIndex, List<TrackLane> lanes)
    {
        var piece = GetPiece(pieceIndex);
        if (!piece.angle.HasValue || !piece.radius.HasValue)
        {
            return Double.MaxValue;
        }
        var lane = lanes.First(l => l.index == laneIndex);
        if (lane == null)
        {
            return 0.0;
        }
        if (MaxVelocities.ContainsKey(pieceIndex) && MaxVelocities[pieceIndex].ContainsKey(laneIndex))
        {
            return MaxVelocities[pieceIndex][laneIndex];
        }
        var radiusForLane = piece.RadiusForLane(laneIndex, lanes);
        return Math.Sqrt(maxCentripetalAcceleration*radiusForLane);
    }

    private double CalculateNextVelocity(double currentVelocity, double throttle, int ticksInTheFuture = 0)
    {
        double currentAccelerationFactor = accelerationFactor;
        double currentTopSpeed = topSpeed;

        if (turboOn && turboOnUntil >= latestGameTick + ticksInTheFuture)
        {
            currentAccelerationFactor = currentAccelerationFactor * turboFactor;
            currentTopSpeed = currentTopSpeed * turboFactor;
        }

        var acceleration = (throttle * currentAccelerationFactor) - (currentVelocity / currentTopSpeed) * currentAccelerationFactor;
        return currentVelocity + acceleration;
    }

    private void send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());
	}

	private double CalculateVelocity()
	{
	    double velocity = 0.0;
		if (carPositions.Count() > 1){
            var last = GetMyLastPosition().piecePosition;
			var secondLast = GetMySecondLastPosition().piecePosition;
			if (last.pieceIndex == secondLast.pieceIndex) {
				velocity = last.inPieceDistance - secondLast.inPieceDistance;
			}
			else
			{
			    var secondLastpiece = gameState.data.race.track.pieces[secondLast.pieceIndex];
                var secondLastPieceLength = secondLastpiece.LengthForLane(secondLast.lane.startLaneIndex, secondLast.lane.endLaneIndex, GetLanes());
                velocity = last.inPieceDistance + (secondLastPieceLength - secondLast.inPieceDistance);
            }
		}
	    if (velocities.Any())
	    {
            accelerations.Add(velocity - velocities.Last());
        }
        velocities.Add(velocity);

	    if (velocities.Count == 6)
	    {
            // Crunch some numbers to get possible new accelerationFactor and topSpeed
	        for (int i = 1; i < velocities.Count; i++)
	        {
	            if (Math.Abs(velocities[i] - velocities[i-1]) > 0.001) // threshold value for checking that velocity has changed;
	            {
	                var acceleration = velocities[i] - velocities[i - 1];
                    accelerationFactor = acceleration + velocities[i - 1] / 50;
                    topSpeed = 50 * accelerationFactor;
                    Log.Write(string.Format("Acceleration factor changed to: {0}, top speed set to: {1}", accelerationFactor, topSpeed));
	                break;
	            }
	        }
	    }

	    return velocity;
	}

    private Piece GetPiece(int pieceIndex)
    {
        return gameState.data.race.track.pieces[pieceIndex];
    }

    private double GetCentripetalAcceleration()
    {
        if (carPositions.Count == 0 || velocities.Count == 0)
        {
            return 0.0;
        }
        var pos = GetMyLastPosition();
        double v2 = Math.Pow(velocities.Last(), 2.0);
        int r = gameState.data.race.track.pieces[pos.piecePosition.pieceIndex].RadiusForLane(pos.piecePosition.lane.endLaneIndex, gameState.data.race.track.lanes);
        if (r < 0.001)
        {
            return 0.0;
        }
        return v2 / r;
    }
}

class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
    	this.data = data;
    }
}

abstract class SendMsg {
	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();
}

class Join: SendMsg {
	public string name;
	public string key;
	public string color;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
		this.color = "red";
	}

	protected override string MsgType() { 
		return "join";
	}
}

class CreateRace : SendMsg
{
    public BotId botId;
    public string trackName;

    public CreateRace(string name, string key, string trackName)
    {
        this.botId = new BotId() {key = key, name = name};
        this.trackName = trackName;
    }

    protected override string MsgType()
    {
        return "createRace";
    }
}

class BotId
{
    public string name;
    public string key;
}

class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

class Throttle: SendMsg {
	public double value;

	public Throttle(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}

class Turbo : SendMsg
{
	public string value;

    public Turbo(string value)
    {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
        return "turbo";
	}
}

class Switch: SendMsg {
	public string value;

	public Switch(string value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "switchLane";
	}
}


//// LOGGING

public static class Log {
	public static string Filename;
    private static string LogMessage;
    private static bool writeToFile = false;

	public static void Write(string msg)
	{
	    LogMessage += msg + Environment.NewLine;
        Console.WriteLine(msg);
	}

    public static void Flush()
    {
        if (writeToFile)
        {
            using (StreamWriter writer = new StreamWriter(Filename, true))
            {
                writer.WriteLine(LogMessage);
            }
        }
        LogMessage = "";
    }

    public static void WriteToFile(bool enabled)
    {
        writeToFile = enabled;
    }
}




// Message types

public class Piece
{
    public double length { get; set; }
    public bool? @switch { get; set; }
    public int? radius { get; set; }
    public double? angle { get; set; }

    public bool HasSwitch()
    {
        return @switch.HasValue && @switch.Value;
    }

    public double LengthForLane(int laneIndex, List<TrackLane> lanes)
    {
        if (Math.Abs(length) > 0.001)
        {
            return length;
        }
        
        if (angle.HasValue && radius.HasValue)
        {
            // Curve piece
            var lane = lanes.FirstOrDefault(l => l.index == laneIndex);
            if (lane == null)
            {
                return 0.0;
            }
            var totalRadius = RadiusForLane(laneIndex, lanes);
            var calculatedLength = Math.Abs(totalRadius*2.0*Math.PI*(angle.Value/360.0));
            //Log.Write(string.Format("Lane: {7}, radius: {0}, angle: {1}, totalRad: {2}, Pi: {3}, trX2: {4}, ang / : {5}, length: {6}, mac speed: {8}", radius.Value, angle.Value, totalRadius, Math.PI, totalRadius * 2.0, (angle.Value / 360.0), calculatedLength, laneIndex, MaxVelocityForLane(laneIndex, lanes, maxCentripetalAcceleration)));
            return calculatedLength;
        }
        
        return 0.0;
    }

    public int RadiusForLane(int laneIndex, List<TrackLane> lanes)
    {
        var lane = lanes.FirstOrDefault(l => l.index == laneIndex);
        if (lane == null)
        {
            return 0;
        }
        if (radius.HasValue && angle.HasValue)
        {
            return radius.Value + (angle.Value > 0.0 ? -lane.distanceFromCenter : lane.distanceFromCenter);
        }
        return 0;
    }

    public bool IsStraight()
    {
        return !angle.HasValue || !radius.HasValue;
    }

    public double LengthForLane(int startIndex, int endIndex, List<TrackLane> lanes)
    {
        // If startLaneIndex and endLaneIndex are different, then calculate lane length for switch piece
        if (startIndex != endIndex)
        {
            var averageLength = (LengthForLane(startIndex, lanes) + LengthForLane(endIndex, lanes)) / 2;
            var distanceBetweenLanes = DistanceBetweenLanes(startIndex, endIndex, lanes);
            // c from pythagora's
            const double switchLengthFactor = 0.96048; // How long the switch is when 1 is the full length of the piece
            var c = Math.Sqrt(Math.Pow(averageLength * switchLengthFactor, 2.0) + Math.Pow(distanceBetweenLanes, 2.0));
            var laneDistanceEnds = averageLength * (1 - switchLengthFactor); // parts of the lane that are not part of the switch
            return c + laneDistanceEnds;
        }
        return LengthForLane(endIndex, lanes);
    }

    private double DistanceBetweenLanes(int lane1, int lane2, List<TrackLane> lanes)
    {
        return Math.Abs(lanes.First(l => l.index == lane1).distanceFromCenter - lanes.First(l => l.index == lane2).distanceFromCenter);
    }
}

public class TrackLane
{
    public int distanceFromCenter { get; set; }
    public int index { get; set; }
}

public class Position
{
    public double x { get; set; }
    public double y { get; set; }
}

public class StartingPoint
{
    public Position position { get; set; }
    public double angle { get; set; }
}

public class Track
{
    public string id { get; set; }
    public string name { get; set; }
    public List<Piece> pieces { get; set; }
    public List<TrackLane> lanes { get; set; }
    public StartingPoint startingPoint { get; set; }

    public Piece GetNextPiece(int currentPieceIndex, out int nextPieceIndex)
    {
        nextPieceIndex = GetNextPieceIndex(currentPieceIndex);
        return pieces[nextPieceIndex];
    }

    public int GetNextPieceIndex(int currentPieceIndex)
    {
        if (pieces.Count > currentPieceIndex + 1)
        {
            return currentPieceIndex + 1;
        }
        return 0;
    }
}

public class Id
{
    public string name { get; set; }
    public string color { get; set; }
}

public class Dimensions
{
    public double length { get; set; }
    public double width { get; set; }
    public double guideFlagPosition { get; set; }
}

public class Car
{
    public Id id { get; set; }
    public Dimensions dimensions { get; set; }
}

public class RaceSession
{
    public int durationMs { get; set; }
    public int laps { get; set; }
    public int maxLapTimeMs { get; set; }
    public bool quickRace { get; set; }
}

public class Race
{
    public Track track { get; set; }
    public List<Car> cars { get; set; }
    public RaceSession raceSession { get; set; }
}

public class RaceData
{
    public Race race { get; set; }
}

public class GameState
{
    public string msgType { get; set; }
    public RaceData data { get; set; }
}



/// Your car message
public class YourCarData
{
    public string name { get; set; }
    public string color { get; set; }
}

public class YourCarMessage
{
    public string msgType { get; set; }
    public YourCarData data { get; set; }
}



/// Car positions message
public class Lane
{
    public int startLaneIndex { get; set; }
    public int endLaneIndex { get; set; }
}

public class PiecePosition
{
    public int pieceIndex { get; set; }
    public double inPieceDistance { get; set; }
    public Lane lane { get; set; }
    public int lap { get; set; }
}

public class CarPosData
{
    public Id id { get; set; }
    public double angle { get; set; }
    public PiecePosition piecePosition { get; set; }
}

public class CarPositions
{
    public string msgType { get; set; }
    public List<CarPosData> data { get; set; }
    public string gameId { get; set; }
    public int gameTick { get; set; }
}

public class FinishedCar
{
    public string name { get; set; }
    public string color { get; set; }
}

public class LapTime
{
    public int lap { get; set; }
    public int ticks { get; set; }
    public int millis { get; set; }
}

public class RaceTime
{
    public int laps { get; set; }
    public int ticks { get; set; }
    public int millis { get; set; }
}

public class Ranking
{
    public int overall { get; set; }
    public int fastestLap { get; set; }
}

public class LapFinishedData
{
    public FinishedCar car { get; set; }
    public LapTime lapTime { get; set; }
    public RaceTime raceTime { get; set; }
    public Ranking ranking { get; set; }
}

public class LapFinished
{
    public string msgType { get; set; }
    public LapFinishedData data { get; set; }
    public string gameId { get; set; }
    public int gameTick { get; set; }
}

public class CrashData
{
    public string name { get; set; }
    public string color { get; set; }
}

public class CrashMessage
{
    public string msgType { get; set; }
    public CrashData data { get; set; }
    public string gameId { get; set; }
    public int gameTick { get; set; }
}

public class TurboData
{
    public double turboDurationMilliseconds { get; set; }
    public int turboDurationTicks { get; set; }
    public double turboFactor { get; set; }
}

public class TurboAvailable
{
    public string msgType { get; set; }
    public TurboData data { get; set; }
}

public class SpawnData
{
    public string name { get; set; }
    public string color { get; set; }
}

public class Spawn
{
    public string msgType { get; set; }
    public SpawnData data { get; set; }
    public string gameId { get; set; }
    public int gameTick { get; set; }
}

public class TurboStartData
{
    public string name { get; set; }
    public string color { get; set; }
}

public class TurboStart
{
    public string msgType { get; set; }
    public TurboStartData data { get; set; }
    public int gameTick { get; set; }
}

public class TurboEndData
{
    public string name { get; set; }
    public string color { get; set; }
}

public class TurboEnd
{
    public string msgType { get; set; }
    public TurboEndData data { get; set; }
    public int gameTick { get; set; }
}